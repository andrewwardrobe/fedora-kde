# frozen_string_literal: true

require 'spec_helper'
require 'json'

describe 'fedora KDE Vagrant Image' do
  include_examples 'base::system'
  include_examples 'packages::iac_tools'
  include_examples 'packages::ruby'
  include_examples 'packages::node'

  # https://www.chef.io/blog/2015/02/26/bento-box-update-for-centos-and-fedora/

end
