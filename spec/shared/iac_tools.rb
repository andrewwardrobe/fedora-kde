# frozen_string_literal: true

shared_examples 'packages::iac_tools' do
  describe 'DevOps Tooling' do
    it 'Should have terraform installed' do
      expect(file('/usr/local/bin/terraform')).to exist
    end

    it 'Should have packer installed' do
      expect(file('/usr/local/bin/packer')).to exist
    end

    it 'Should have puppet installed' do
      expect(package('puppet')).to be_installed
    end

    it 'Should have docker installed' do
      expect(package('docker-ce')).to be_installed
      expect(package('docker-compose')).to be_installed
      expect(service('docker')).to be_running
    end

    it 'Should have ansible installed' do
      expect(package('ansible')).to be_installed
    end
  end
end
