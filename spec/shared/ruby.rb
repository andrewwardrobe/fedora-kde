# frozen_string_literal: true

shared_examples 'packages::ruby' do
  describe 'Ruby Tools' do
    packages = %w[ruby ruby-devel]
    gems = %w[bundle rails serverspec]

    packages.each do |pkg|
      it "#{pkg} should be installed" do
        expect(package(pkg)).to be_installed
      end
    end

    gems.each do |gem|
      it "#{gem} should be installed" do
        expect(package(gem)).to be_installed.by 'gem'
      end
    end

    ruby_deps = ['zlib', 'zlib-devel', 'gcc-c++', 'patch', 'readline', 'readline-devel', 'libyaml-devel',
                 'libffi-devel', 'openssl-devel', 'make', 'bzip2', 'autoconf', 'automake', 'libtool', 'bison',
                 'sqlite-devel', 'gcc', 'redhat-rpm-config']

    ruby_deps.each do |dep|
      it "#{dep} should be installed" do
        expect(package(dep)).to be_installed
      end
    end
  end
end
