shared_examples 'script::containers' do
  describe docker_container('nginx') do
    it { is_expected.to exist }
    it { is_expected.to be_running }
  end
  describe docker_container('jenkins') do
    it { is_expected.to exist }
    it { is_expected.to be_running }
  end
end