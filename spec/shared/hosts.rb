shared_examples 'script::nginx' do
  describe host('jenkins.local') do
    it { is_expected.to be_resolvable.by('hosts') }
    its(:ipaddress) { is_expected.to eq '127.0.0.1' }
    it { is_expected.to be_reachable }
  end

  describe command('curl -kL http://jenkins.local/') do
    its(:exit_status) { is_expected.to eq 0 }
  end
  describe command('curl -kL https://jenkins.local/') do
    its(:exit_status) { is_expected.to eq 0 }
  end
end