# frozen_string_literal: true

shared_examples 'packages::java' do
  describe 'Java Development Feature' do
    it 'Should Have a JDK installed' do
      expect(package('java-1.8.0-openjdk-devel')).to be_installed
    end
  end
end
