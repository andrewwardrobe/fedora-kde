# frozen_string_literal: true

shared_examples 'script::setup' do
  context 'With the example data.yaml' do
    describe user('demo') do
      it { should exist }
      it { should belong_to_group 'docker' }
      it { should have_uid 7001 }
      # it { should have_authorized_key  }
      it 'should set the password' do
        expect(user('demo').encrypted_password).to match('$6$LO.XczfsWbiQPCs$89o8I5w7RP.Nu621hzQWy5WRg7P73TXfSYpUuBPv8iheVEX4ClqIqf1dLidC63I/FO535lUiU.Zh9LrICrfHN.')
      end
      it 'should have a id_rsa.pub file' do
        expect(file('/home/demo/.ssh/id_rsa.pub').content).to match 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCvgZGgBewYMJrxRoIT+yoeiR2NON9WxJ0682IDhyMcJe5qxHcTJUKcmD/2RquDspvjzZqszp8dxY4VNyS3d4D9r6fsxc0V0ao7ixX6k4jNoVplzk0mOL6wmCLyhBxdNltHbFSQp3Q8YRXOnjES3NRZhDV1irE1wbTZFAb25gAJE4NoFXAQsXeYSn6QoPBU8iD/MXQJjVSnRFbETgFpVp+Kxqca6eDknUizWnrNRXzccSVT9Fx9Er2NUBxs0WBTSBzjAnwycCJJDsdUB1mstT95aOIwmadjiST5o9DxFqN3ycqhPqQJDj9SqIe7NEHLgwvAewfPnBnvhDRfnTi4v83N example@pubkey'
      end
      it 'should have a test_key file' do
        expect(file('/home/demo/.ssh/test_key')).to exist
        priv = File.read('spec/vagrant/script/keys/test.rsa')
        expect(file('/home/demo/.ssh/test_key').content).to match priv
      end
      it 'should have a test_key.pub file' do
        expect(file('/home/demo/.ssh/test_key.pub')).to exist
        pub = File.read('spec/vagrant/script/keys/test.rsa.pub')
        expect(file('/home/demo/.ssh/test_key.pub').content).to match pub
      end
    end
  end
end

shared_examples 'script::packages' do
  describe package('cowsay') do
    it { should be_installed }
  end

  describe package('json') do
    it { should be_installed.by('gem') }
  end

  # describe package('awscli') do
  #   it { should be_installed.by('pip') }
  # end

end

shared_examples 'script::development_tools' do
  describe file('/opt/idea/idea-ultimate-2019.2/') do
    it { should exist }
    it { should be_directory }
  end
  describe file('/opt/rubymine/RubyMine-2019.1.3/') do
    it { should exist }
    it { should be_directory }
  end
  describe file('/home/demo/.rbenv') do
    it { should exist }
    it { should be_directory }
  end
  describe file('/usr/local/bin/gradle') do
    it { should exist }
    it { should be_symlink }
    it { should be_linked_to '/usr/local/share//gradle-4.4.1/bin/gradle' }
  end
  describe file('/usr/local/bin/mvn') do
    it { should exist }
    it { should be_symlink }
    it { should be_linked_to '/opt/maven/apache-maven-3.5.3/bin/mvn' }
  end
  context "rbenv" do
    describe file('/home/demo/.rbenv/bin') do
      it { should exist }
      it { should be_a_directory }
    end
    describe file('/home/demo/.rbenv/plugins/ruby-build') do
      it { should exist }
      it { should be_a_directory }
    end

    describe file('/home/demo/.rbenv/versions/2.4.0') do
      it { should exist }
      it { should be_a_directory }
    end

    describe file('/home/demo/.rbenv/versions/2.5.0') do
      it { should exist }
      it { should be_a_directory }
    end
  end
end
