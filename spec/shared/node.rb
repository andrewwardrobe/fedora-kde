# frozen_string_literal: true

shared_examples 'packages::node' do
  describe 'Node Tools' do
    packages = %w[nodejs npm]
    packages.each do |pkg|
      it "#{pkg} should be installed" do
        expect(package(pkg)).to be_installed
      end
    end
  end
end
