# frozen_string_literal: true

require 'spec_helper'
require 'json'

describe 'Setup Script' do
  include_examples 'base::system'
  include_examples 'packages::iac_tools'
  include_examples 'packages::ruby'
  include_examples 'packages::node'
  include_examples 'script::setup'
  include_examples 'script::packages'
  include_examples 'script::development_tools'
  include_examples 'script::containers'
  include_examples 'script::nginx'



  # https://www.chef.io/blog/2015/02/26/bento-box-update-for-centos-and-fedora/
end
