module Serverspec::Type
    class DockerContainer

        def include_regex?
          inspection.find { |str| str =~ regex }
        end

        def have_image?(image)
          inspection['Config']['Image'] == image
        end
        def has_environment_variable?(regex)
          inspection['Config']['Env'].find { |str| str =~ /^#{regex}=/ }
        end

        private
        def get_inspection
          @containers = @runner.run_command("docker ps -qa -f Name=#{@name}").stdout unless @containers
          @get_inspection ||= @runner.run_command("docker inspect #{@containers}")
        end
  end
end

def container_inspection(filter)
  img_cmd = "docker ps -qa -f #{filter}"
  images = command(img_cmd).stdout
  cmd = "docker inspect #{images}"
  data = command(cmd).stdout
  JSON.parse(data)
end

RSpec::Matchers.define :include_regex do |regex|
  match do |actual|
    actual.include_regex? regex
  end
end

RSpec::Matchers.define :have_environment_variable do |regex|
  match do |actual|
    actual.has_environment_variable? regex
  end
end


RSpec::Matchers.define :have_running_state do
  match do |actual|
    actual['State']['Running'] == true
  end
  description do
    'be running'
  end
end

RSpec::Matchers.define :have_image do |image|
  match do |actual|
    actual.have_image?(image)
  end
end
RSpec::Matchers.define :have_image_sha do |sha|
  match do |actual|
    actual['Image'] == sha
  end
end

RSpec::Matchers.define :run_as_user do |user|
  match do |actual|
    actual['Config']['User'] == user
  end
end

RSpec::Matchers.define :have_user do |user|
  match do |actual|
    actual['Config']['User'] == user
  end
end

RSpec::Matchers.define :have_hostname do |hostname|
  match do |actual|
    actual['Config']['Hostname'] == hostname
  end
end

RSpec::Matchers.define :have_restart_policy do |policy|
  match do |actual|
    actual['HostConfig']['RestartPolicy']['Name'] == policy
  end
end

RSpec::Matchers.define :have_restart_limit do |count|
  match do |actual|
    actual['HostConfig']['RestartPolicy']['MaximumRetryCount'] == count
  end
end

RSpec::Matchers.define :have_domain_name do |domain|
  match do |actual|
    actual['Config']['Domainname'] == domain
  end
end

RSpec::Matchers.define :map_port do |container, host, mode = 'tcp'|
  match do |actual|
    actual['HostConfig']['PortBindings']["#{container}/#{mode}"][0]['HostPort'] == host
  end
  description do
    "map container port #{container} to host port #{host}"
  end
end