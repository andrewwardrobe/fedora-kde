
# frozen_string_literal: true

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)

require 'serverspec'
require 'rspec'

require 'net/ssh'
require 'tempfile'
require 'yaml'
require 'rubygems'
require 'docker'
require 'bundler'

require 'serverspec_launcher/spec_helper'
require 'support/docker_matchers'
