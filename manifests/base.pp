$packer_version = '1.4.2'
$terraform_version = '0.12.5'
$ansible_version = '2.7.2'

yumrepo{'docker-ce':
  name     => 'docker-ce-stable',
  baseurl  => 'https://download.docker.com/linux/centos/7/$basearch/stable',
  enabled  => 1,
  gpgcheck => 1,
  gpgkey   => 'https://download.docker.com/linux/centos/gpg',
}



$util_packages = [
  'vim-enhanced',
  'curl',
  'wget'
]
$dev_packages = [
  'java-1.8.0-openjdk-devel',
  'ruby',
  'rubygems',
  'ruby-devel',
  'nodejs',
  'npm',
  'kernel-devel',
  'git',
  'python2-pyOpenSSL',
]
$iac_packages = [
  'puppet',
  'docker-ce',
  'docker-compose'
]
$ruby_deps = ['zlib',
  'zlib-devel',
  'gcc-c++',
  'patch',
  'readline',
  'readline-devel',
  'libyaml-devel',
  'libffi-devel',
  'openssl-devel',
  'make',
  'bzip2',
  'autoconf',
  'automake',
  'libtool',
  'bison',
  'sqlite-devel',
  'gcc',
  'redhat-rpm-config'
]

$gems = [
  'bundle',
  'serverspec',
  'rails',
  'bundler',
  'puppet-lint'
]

package {'vim-minimal':
  ensure => 'latest'
}

ensure_packages($ruby_deps)
ensure_packages($util_packages)
ensure_packages($dev_packages)
ensure_packages($iac_packages)


# package{$gems:
#  require  => Package[$dev_packages],
#  provider => 'gem'
# }

service{'docker':
  ensure => 'running',
  enable => true
}

file{'/opt/idea':
  ensure => 'directory',
}

package{'ansible': }

file_line{'ansible_callback':
  line  => 'callback_plugins   = ~/ansible/plugins/callback:/usr/share/ansible/plugins/callback',
  match => 'callback_plugins   = /usr/share/ansible/plugins/callback',
  path  => '/etc/ansible/ansible.cfg',
  require => Package['ansible'],
}

file{'/usr/share/locale/config.yaml':
  ensure  => present,
  content => "---\ngettext:\n",
  replace => false,
}

archive{'terraform':
  ensure        => present,
  path          => '/tmp/terraform.zip',
  cleanup       => true,
  extract       => true,
  extract_path  => '/usr/local/bin',
  creates       => '/usr/local/bin/terraform',
  source        => "https://releases.hashicorp.com/terraform/${terraform_version}/terraform_${terraform_version}_linux_amd64.zip",
}

archive{'packer':
  ensure       => present,
  path         => '/tmp/packer.zip',
  cleanup      => true,
  extract      => true,
  extract_path => '/usr/local/bin',
  creates      => '/usr/local/bin/packer',
  source       => "https://releases.hashicorp.com/packer/${packer_version}/packer_${packer_version}_linux_amd64.zip",
}
