
# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'fedora-kde-setup/version'

Gem::Specification.new do |spec|
  spec.name          = 'awar-fedora-kde-setup'
  spec.version       = FedoraKDESetup::VERSION
  spec.authors       = ['Andrew Wardrobe']
  spec.email         = ['andrew.g.wardrobe@googlemail.com']

  spec.summary       = 'Set Up Script for andrewwardrobe/fedora-kde vagrant box'
  spec.description   = ''
  spec.homepage      = 'https://gitlab.com/andrewwardrobe/fedora-kde'
  spec.license       = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features|http|bin|fedora.*json|properties.yml|Puppetfile)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/box-setup}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.13'
  spec.add_development_dependency 'docker-api', '~> 1.34'
  spec.add_development_dependency 'rake', '~> 12.0'
  spec.add_development_dependency 'rspec', '~> 3.6'
  spec.add_development_dependency 'rspec-tick-formatter', '~> 0.1.3'
  spec.add_development_dependency 'rspec_junit_formatter', '~> 0.3'
  spec.add_development_dependency 'rspec_html_reporter','~> 1.0.0'
  spec.add_development_dependency 'serverspec', '~> 2.41'
  spec.add_development_dependency 'serverspec_launcher', '0.8.0'


  spec.add_runtime_dependency 'librarian-puppet', '~> 3.0'
  spec.add_runtime_dependency  'puppet-lint', '~> 2.3', '>= 2.3.3'
  spec.add_runtime_dependency  'rake', '~> 12.3'
  spec.add_runtime_dependency  'json', '~> 2.1'
end
