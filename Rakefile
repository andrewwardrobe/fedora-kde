# frozen_string_literal: true

require 'bundler/gem_tasks'
require 'rake'
require 'rspec/core/rake_task'
require 'yaml'
require 'serverspec_launcher/rake_tasks'
require 'fedora-kde-setup/symbolize_helper'

using SymbolizeHelper

config = YAML.load_file('properties.yml').deep_symbolize_keys
boxname = config[:variables][:boxname]

boxfile = "box/virtualbox/#{config[:variables][:boxfile]}"

RSpec::Core::RakeTask.new(:spec)

task default: :spec

task spec: 'spec:all'

task :clean_base do
  system 'cd spec/vagrant/base; vagrant destroy -f'
end

task :clean_script do
  system 'cd spec/vagrant/script; vagrant destroy -f'
end

desc 'Cleanup'
task :clean do
  system 'cd spec/vagrant/base; vagrant destroy -f'
  system 'cd spec/vagrant/script; vagrant destroy -f'
  system "vagrant box remove  #{boxname}"
end

namespace :vagrant do
  desc 'Run vagrant up on the script test box'
  task :script_destroy do
    system 'cd spec/vagrant/script; vagrant destroy -f'
  end

  desc 'Run vagrant up on the script test box'
  task :script_up do
    system 'cd spec/vagrant/script; vagrant up --provision'
  end
  desc 'Run vagrant ssh on the script test box'
  task :script_ssh do
    system 'cd spec/vagrant/script; vagrant ssh'
  end


  task :box_destroy do
    system 'cd spec/vagrant/box; vagrant destroy -f'
  end

  desc 'Run vagrant up on the box test box'
  task :box_up do
    system 'cd spec/vagrant/box; vagrant up --provision'
  end
  desc 'Run vagrant ssh on the box test box'
  task :box_ssh do
    system 'cd spec/vagrant/box; vagrant ssh'
  end
  desc 'Run vagrant up on the script test box'
  task :base_destroy do
    system 'cd spec/vagrant/base; vagrant destroy -f'
  end

  desc 'Run vagrant up on the script test box'
  task :base_up do
    system 'cd spec/vagrant/base; vagrant up --provision'
  end
end

namespace :box do
  desc 'Builds The Box'
  task :build do
    system 'librarian-puppet install'
    system 'packer build -only=virtualbox-iso -force --var-file=fedora27-ws.json fedora.json'
  end
  desc 'Builds The Box'
  task :build28 do
    system 'librarian-puppet install'
    system 'packer build -only=virtualbox-iso -force --var-file=fedora28-ws.json fedora.json'
  end
  desc 'Builds The Box'
  task :build30 do
    system 'librarian-puppet install'
    system 'packer build -only=virtualbox-iso -force -on-error=abort --var-file=fedora30-ws.json fedora.json '
  end
  desc 'Removes the box from vagrant'
  task :remove do
    system "vagrant box remove  #{boxname}"
  end
  desc 'Install the vagrant box'
  task :install do
    system "vagrant box add  #{boxname} #{boxfile}"
  end
end

#Rake::Task['serverspec:base'].enhance do
#  Rake::Task[:clean_base].invoke
#end
