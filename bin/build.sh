#!/usr/bin/env bash

build_command="packer build -only=virtualbox-iso -force --var-file=fedora27-ws.json fedora.json"
test_command="bin/test-box box/virtualbox/awar-fedora-0.1.0.box virtualbox"

gem list | grep bundler > /dev/null || { echo -e "need bundler, install with:\n\tgem install bundler" ; exit 1; }

echo "Installing build dependencies ..."
bundle install
echo "Installing Puppet modules ..."
librarian-puppet install

echo "Building Box ..."
${build_command} && ${test_command}