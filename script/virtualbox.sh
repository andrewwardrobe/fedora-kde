#!/bin/bash -eux

SSH_USER=${SSH_USERNAME:-vagrant}
SSH_USER_HOME=${SSH_USER_HOME:-/home/${SSH_USER}}

if [[ $PACKER_BUILDER_TYPE =~ virtualbox ]]; then
    echo "==> Installing VirtualBox guest additions"
    # Some of these packages should already have been installed in the kickstart
    dnf -y install elfutils-libelf-devel
    dnf -y install kernel-headers-"$(uname -r)" kernel-devel-"$(uname -r)" gcc make perl
    # Need to set the KERN_DIR in Fedora 26 and 27 or the VBox additions will not install
    dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
    dnf remove -y virtualbox-guest-additions
    dnf install -y virtualbox-guest-additions akmod-VirtualBox
    akmods-shutdown
    rm -f $SSH_USER_HOME/.vbox_version
fi
