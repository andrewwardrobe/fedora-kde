#!/usr/bin/env ruby

require 'json'
require 'yaml'
require 'open-uri'
require 'rest-client'
require 'nokogiri'

require 'pp'

"https://plugins.jetbrains.com"



class JetBrainsIDE



  def initialize(ide = 'community', platform = 'linux' , version)
    @products = JSON.parse(open('https://data.services.jetbrains.com/products').read)
    @codes = {
        community: 'IIC',
        ultimate: 'IIU',
        rubymine: 'RM'
    }
    @code = @codes[ide.to_sym]
    @version = version
    @platform = platform
    @product_data = get_product_data
  end

  def get_product_data
    @products.find { |x| x['code'] == @code }
  end

  def get_version_data
    @product_data['releases'].select { |x| x['version'] == @version }
  end

  def get_download_data
    data = get_version_data[0]
    {
        build: data['build'],
        version: data['version'],
        link: data['downloads'][@platform]['link']
    }
  end


  def get_plugin_list(build)
    RestClient.get("https://plugins.jetbrains.com/plugins/list?build=#{build}")
  end

end




api = JetBrainsIDE.new('2018.1')

dd  = api.get_download_data

pp dd

#pp Nokogiri::XML(api.get_plugin_list(dd[:build]).body)