# frozen_string_literal: true

require 'fileutils'
require 'yaml'
require 'fedora-kde-setup/hash'
require 'fedora-kde-setup/util'

class AnsibleJenkins
  include Util
  def initialize(config = {}, tempdir = '/tmp/box-setup')
    @default_config = {
      jenkins_version: 'lts',
      jenkins_host: 'localhost',
      jenkins_port: '8080',
      jenkins_admin_user: '',
      jenkins_admin_password: '',
      jenkins_authenticate: 'no',
      jenkins_env: {
        JAVA_OPTS: '-Djenkins.install.runSetupWizard=false'
      },
      plugins: %w[github git credentials scm-api matrix-project git-client
                  github-api ssh-slaves ssh-credentials
                  log-parser copyartifact workflow-aggregator workflow-multibranch
                  ansicolor blueocean]
    }
    if config[:reverse_proxy]
      @default_config[:jenkins_env][:VIRTUAL_HOST] = 'jenkins.local'
      @default_config[:jenkins_env][:VIRTUAL_PORT] = @default_config[:jenkins_port]
      @default_config[:jenkins_env] = deep_stringify_keys(@default_config[:jenkins_env])
    end
    @dir = tempdir
    @config = @default_config.deep_merge(config)
  end

  def build
    Hash[@config.collect { |k, v| [k.to_s, v] }]
  end

  def execute
    system "cd #{@dir}/ansible && ansible-playbook cd.yml"
  end
end
