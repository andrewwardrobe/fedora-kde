# frozen_string_literal: true

module Util
  def transform_hash(original, options = {}, &block)
    original.inject({}){|result, (key,value)|
      value = if (options[:deep] && Hash === value)
                transform_hash(value, options, &block)
              else
                value
              end
      block.call(result,key,value)
      result
    }
  end

  # Convert keys to strings
  def stringify_keys(hash)
    transform_hash(hash) do |hash, key, value|
      hash[key.to_s] = value
    end
  end

  # Convert keys to strings, recursively
  def deep_stringify_keys(hash)
    transform_hash(hash, deep: true) do |hash, key, value|
      hash[key.to_s] = value
    end
  end
end
