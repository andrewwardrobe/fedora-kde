# frozen_string_literal: true

class PuppetBuilder
  def initialize(config = {})
    @nfs_config = {
      ensure: 'present',
      atboot: 'false',
      options: '_netdev,user,noauto',
      type: 'nfs'
    }

    @bind_config = {
      ensure: 'mounted',
      atboot: 'true',
      options: 'rw,bind',
      type: 'none'
    }

    @rubymine = {
      title: 'rubymine',
      path: '/tmp/rubymine.tar.gz',
      extract_to: '/opt/rubymine',
      source: 'https://download.jetbrains.com/ruby/RubyMine-2018.1.3.tar.gz',
      creates: '/opt/rubymine/RubyMine-2018.1.3'
    }
    @puppet_file = '/tmp/extras.pp'
    @config = config
  end

  def host_entry(title, hash)
    bnd = binding
    bnd.local_variable_set(:title, title)
    bnd.local_variable_set(:hash, hash)
    template = <<~HEREDOC
        host{'<%= title %>':
          ip => '<%= hash[:ip] %>',
          <% if hash[:aliases] %>
          host_aliases => <%= hash[:aliases] %>,
          <% end %>
      }
    HEREDOC
    ERB.new(template, 1, '>').result(bnd)
  end

  def build
    File.open(@puppet_file, 'w') do |file|
      rbconf = @config.dig(:ides, :rubymine).is_a?(TrueClass) ? {} : @config.dig(:ides, :rubymine)
      file.write archive_heredoc(@rubymine, rbconf) if install_rubymine
      if @config[:mounts] && $options[:mounts]
        @config[:mounts][:nfs].each do |data|
          file.write mount_heredoc(data, @nfs_config)
        end
        @config[:mounts][:bind].each do |data|
          file.write mount_heredoc(data, @bind_config)
        end
      end
      @config[:hosts]&.each do |key, val|
        file.write host_entry(key, val)
      end
    end
  end

  def apply
    puts 'Running Puppet'
    system "puppet apply #{@puppet_file}"
  end

  def bind_mount(data)
    mount_heredoc(data, @bind_config)
  end

  def execute(puppet_code)
    File.open('/tmp/puppet.pp', 'w') do |file|
      file.write puppet_code
    end
    system 'puppet apply /tmp/puppet.pp'
  end

  def bind_dir(target, newtarget)
    execute bind_mount('mountpoint' => newtarget, 'device' => target)
  end

  def user(user, details)
    execute user_heredoc(user, details)
  end

  def archive_heredoc(options, config)
    <<~HEREDOC
      file {'#{options[:extract_to]}':
        ensure => 'directory'
      }
      archive{'#{options[:title]}':
        ensure        => present,
        path          => '#{options[:path]}',
        cleanup       => true,
        extract       => true,
        extract_path  => '#{options[:extract_to]}',
        creates       => '#{options[:creates]}',
        source        => '#{options[:source]}',
        require       => File['#{options[:extract_to]}']
      }
      \$#{options[:title].downcase}_plugins = #{config[:plugins] ? config[:plugins].to_s : '{}'}
      \$#{options[:title].downcase}_plugins.each  | $plugin, $url | {
        archive{"${title}_${plugin}_plugin":
          ensure        => present,
          path          => "/tmp/${title}_${plugin}.zip",
          cleanup       => true,
          extract       => true,
          extract_path  => '#{options[:creates]}/plugins/',
          creates       => "#{options[:creates]}/plugins/${plugin}",
          source        => $url,
        }
      }

    HEREDOC
  end

  def mount_heredoc(options, config)
    <<~HEREDOC
      file{'#{options['mountpoint']}':
        ensure => directory,
        mode => '0777'
      }
      mount{'#{options['mountpoint']}':
        ensure  => '#{config[:ensure]}',
        atboot  => '#{config[:atboot]}',
        fstype  => '#{config[:type]}',
        device  => '#{options['device']}',
        options => '#{config[:options]}',
      }
    HEREDOC
  end

  def user_heredoc(login, user)
    passwd = user[:passwd] ? "password => '#{user[:passwd]}'," : ''
    uid = user[:uid] ? "uid => '#{user[:uid]}'," : ''
    <<~HEREDOC
      user{'#{login}':
        ensure     => present,
        comment    => '#{user[:name]}',
        #{passwd}
        managehome => true,
        #{uid}
        groups     => ['wheel', 'docker'],
      }
    HEREDOC
  end

  private

  def install_rubymine
    @config.dig(:ides, :rubymine) && $options[:rubymine]
  end
end
