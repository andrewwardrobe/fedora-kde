# frozen_string_literal: true

require 'fileutils'
require 'yaml'
require 'fedora-kde-setup/hash'
require 'fedora-kde-setup/util'

class AnsiblePackages
  include Util
  attr_reader :config

  def initialize(config)
    cfg = {
      name: 'Install Packages',
      package: {
        name: '{{ item }}',
        state: 'present'
      },
      with_items: config.dig(:packages) || []
    }
    @config = cfg.deep_stringify_keys
  end
end
