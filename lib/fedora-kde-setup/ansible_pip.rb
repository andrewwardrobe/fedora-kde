# frozen_string_literal: true

require 'fileutils'
require 'yaml'
require 'fedora-kde-setup/hash'
require 'fedora-kde-setup/util'

class AnsiblePip
  include Util
  attr_reader :config

  def initialize(config)
    cfg = [ deep_stringify_keys(
      name: 'Install pip',
      package: {
        name: 'python2-pip',
        state: 'present'
      }
    ), deep_stringify_keys(
      name: 'Install Python Modules (pip)',
      pip: {
        name: '{{ item }}',
        state: 'present'
      },
      with_items: config.dig(:pip) || []
    )]
    @config = cfg
  end
end
