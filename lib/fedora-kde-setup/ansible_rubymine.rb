# frozen_string_literal: true

require 'fileutils'
require 'yaml'
require 'fedora-kde-setup/hash'
require 'fedora-kde-setup/util'

class AnsibleRubymine
  include Util
  attr_reader :config

  def initialize(config)
    cfg = {
      rubymine_version: config.safe_dig(:ides, :rubymine, :version) || '2019.1.3'
    }
    users = []
    config[:users].each do |idx, user|
      ide_config = user.dig(:ides, :rubymine)
      users << user_config(ide_config, idx) if ide_config
    end

    cfg[:users] = users
    @config = cfg.deep_stringify_keys
  end

  def user_config(config, user)
    conf = {
      username: user.to_s
    }

    conf[:rubymine_plugins] = config[:plugins] if config[:plugins]
    conf
  end
end
