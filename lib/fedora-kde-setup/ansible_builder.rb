# frozen_string_literal: true

require 'fileutils'
require 'yaml'
require 'fedora-kde-setup/hash'
require 'fedora-kde-setup/ansible_jenkins'
require 'fedora-kde-setup/ansible_intellij'
require 'fedora-kde-setup/ansible_rubymine'
require 'fedora-kde-setup/ansible_maven'
require 'fedora-kde-setup/ansible_gradle'
require 'fedora-kde-setup/ansible_package'
require 'fedora-kde-setup/ansible_rbenv'
require 'fedora-kde-setup/ansible_gem'
require 'fedora-kde-setup/ansible_nginx'
require 'fedora-kde-setup/ansible_pip'

class AnsibleBuilder
  require 'erb'

  attr_reader :host_vars, :plays
  def make_playbook(config)
    play = {
      hosts: 'localhost',
      remote_user: 'vagrant',
      become: true
    }
    play[:tasks] = []
    play[:tasks] << AnsiblePackages.new(config).config if config.dig(:packages)
    play[:tasks] += AnsiblePip.new(config).config if config.dig(:pip)
    play[:tasks] += AnsibleNginx.new(config).config if config.dig(:reverse_proxy)
    play[:tasks] << AnsibleGems.new(config).config if config.dig(:gems)

    play[:roles] = %w[base java docker]
    play[:roles] << 'jenkins' if config.safe_dig(:jenkins)
    play[:roles] << 'gantsign.maven' if config.safe_dig(:'build-tools', :maven)
    play[:roles] << 'gantsign.maven-color' if config.safe_dig(:'build-tools', :maven)
    play[:roles] << 'shelleg.gradle' if config.safe_dig(:'build-tools', :gradle)
    play[:roles] << 'gantsign.intellij' if config.safe_dig(:ides, :intellij)
    play[:roles] << 'rubymine' if config.safe_dig(:ides, :rubymine)
    play[:roles] << 'zzet.rbenv' if config.safe_dig(:rbenv)

    @plays = [play.deep_stringify_keys]
  end

  def initialize(config, tempdir = '/tmp/box-setup')
    @dir = tempdir
    jenkins_config = config.safe_dig(:jenkins) ||  {}
    jenkins = AnsibleJenkins.new(jenkins_config).build
    intellij = AnsibleIntellij.new(config).config
    rubymine = AnsibleRubymine.new(config).config
    maven = AnsibleMaven.new(config).config
    gradle = AnsibleGradle.new(config).config
    rbenv = AnsibleRBEnv.new(config).config
    @host_vars = jenkins.merge(intellij).merge(rubymine).merge(maven).merge(gradle).merge(rbenv)
    make_playbook config
  end

  def build
    FileUtils.mkpath @dir unless Dir.exist? @dir
    path = File.expand_path('../../templates', __dir__)
    Dir["#{path}/ansible/*"]
    puts "Copying #{path} to #{@dir}"
    FileUtils.cp_r "#{path}/ansible", @dir

    FileUtils.mkpath "#{@dir}/ansible/host_vars" unless Dir.exist? "#{@dir}/ansible/host_vars"
    File.open("#{@dir}/ansible/host_vars/localhost.yml", 'w') do |file|
      file.write(@host_vars.to_yaml)
    end
    File.open("#{@dir}/ansible/cd.yml", 'w') do |file|
      file.write(@plays.to_yaml)
    end
  end

  def execute
    system "cd #{@dir}/ansible && ansible-galaxy install -r requirements.yml"
    system "cd #{@dir}/ansible && ansible-playbook -i 'localhost,' -c local cd.yml -e 'ansible_python_interpreter=/usr/bin/python3'"
  end
end
