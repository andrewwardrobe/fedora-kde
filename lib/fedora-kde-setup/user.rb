
# frozen_string_literal: true

require 'fileutils'

class Hash
  def to_s
    '{' + keys.inject([]) do |a, key|
      a << "'#{key}' => '#{fetch(key)}'"
    end.join(', ') + '}'
  end
end

class UserSetup
  def initialize(users = [])
    @users = users
  end

  def do_as(user)
    # Find the user in the password database.
    u = user.is_a?(Integer) ? Etc.getpwuid(user) : Etc.getpwnam(user)
    pid = Process.fork do
      # We're in the child. Set the process's user ID.
      Process.uid = u.uid
      # Invoke the caller's block of code.
      yield(user)
    end
    Process.waitpid pid
    $CHILD_STATUS.exitstatus
  end

  def git_config(user, details)
    Dir.chdir('/tmp') do
      system("sudo -u #{user} git config --global user.name \"#{details[:name]}\"")
      system("sudo -u #{user} git config --global user.email \"#{details[:email]}\"")
    end
  end

  def dotfile_script(user, dotfile_repo)
    <<~HEREDOC
      cd ~#{user}
      echo ".cfg" >> .gitignore
      git clone --bare #{dotfile_repo} \$HOME/.cfg > /dev/null 2>&1
       alias config="/usr/bin/git --git-dir=\$HOME/.cfg/ --work-tree=\$HOME"
       mkdir -p .config-backup
      config checkout > /dev/null 2>&1
      res=$?
      if [ $res = 0 ]; then
        echo "Checked out config.";
        else
          echo "Backing up pre-existing dot files.";
          config checkout 2>&1 | egrep "\\s+\\." | awk {'print $1'} | xargs -I{} mv {} .config-backup/{}
      fi;
      config checkout > /dev/null
      config config status.showUntrackedFiles no
      sed -i 's/%USER%/#{user}/g' .bashrc
    HEREDOC
  end

  def dotfiles(user, repo)
    puts "Downloading Dotfiles from #{repo}"
    do_as user do
      script = dotfile_script(user, repo)
      env = { 'HOME' => "/home/#{user}" }
      system env, script
    end
  end

  def create_user(user, details)
    puts "Creating User: #{user}"
    PuppetBuilder.new.user(user, details)
    shadow = File.read('/etc/shadow')
    File.open('/etc/shadow', 'w') do |file|
      file.write shadow.gsub(/^#{user}:!!/, "#{user}:#{details[:passwd]}")
    end
  end

  def validate_user(user, details)
    puts "Validating #{user}:"
    errs = 0
    unless details[:name]
      puts "\t Needs name"; errs += 1
    end
    unless details[:email]
      puts "\t Needs Email"; errs += 1
    end

    unless details[:uid]&.is_a?(Integer)
      puts "\t Needs a numeric uid"; errs += 1
    end
    errs
  end

  def do_ssh(user, details)
    path = File.expand_path "~#{user}/.ssh/"
    Dir.mkdir(path) unless Dir.exist? path
    ssh_keys = details[:ssh_keys] || []
    ssh_keys.each do |key|
      keyname = key['name'] ? key['name'] : 'id_rsa'

      if key['id_rsa']
        puts "Writing #{path}/#{keyname} from inline key"
        File.open("#{path}/#{keyname}", 'w') { |file| file.write key['id_rsa'] }
      end
      if key['private_key']
        puts "Writing #{path}/#{keyname} from inline key"
        File.open("#{path}/#{keyname}", 'w') { |file| file.write key['private_key'] }
      end
      if key['public_key']
        puts "Writing #{path}/#{keyname}.pub from inline key"
        File.open("#{path}/#{keyname}.pub", 'w') { |file| file.write key['public_key'] }
      end
      if key['id_rsa_file']
        key_src = (key['id_rsa_file'].start_with?('/') ? '' : '/vagrant/')
        puts "KEY_SRC = #{key_src}"
        puts "Writing #{path}/#{keyname} from file key"
        FileUtils.copy("#{key_src}/#{key['id_rsa_file']}", "#{path}/#{keyname}")
      end
      if key['private_key_file']
        key_src = (key['private_key_file'].start_with?('/') ? '' : '/vagrant/')
        puts "Writing #{path}/#{keyname} from file key"
        FileUtils.copy("#{key_src}#{key['private_key_file']}", "#{path}/#{keyname}")
      end
      next unless key['public_key_file']
      key_src = (key['public_key_file'].start_with?('/') ? '' : '/vagrant/')
      puts "Writing #{path}/#{keyname}.pub from inline key"
      FileUtils.copy("#{key_src}#{key['public_key_file']}", "#{path}/#{keyname}.pub")
    end
    ssh_config = details[:ssh_config]
    if ssh_config
      File.open("#{path}/config", 'w') do |file|
        cfg = ssh_config[:config] || []
        cfg.each do |key, val|
          file.write "#{key} #{val}\n"
        end

        hosts = ssh_config[:hosts] || []
        hosts.each do |key, val|
          file.write "Host #{key}\n"
          val.each do |k, v|
            file.write "  #{k} #{v}\n"
          end
        end
      end
    end
    FileUtils.chown_R(user, user, path)
    FileUtils.chmod_R(0o700, path)
  end

  def do
    @users.each do |user, details|
      next if validate_user(user, details).positive?
      create_user(user.to_s, details)
      git_config(user.to_s, details)
      do_ssh(user.to_s, details)
      dotfiles(user.to_s, details[:dotfiles]) if details[:dotfiles] && $options[:dotfiles]
    end
  end
end
