# frozen_string_literal: true

require 'fileutils'
require 'yaml'
require 'fedora-kde-setup/hash'
require 'fedora-kde-setup/util'

class AnsibleGradle
  include Util
  attr_reader :config

  def initialize(config)
    cfg = {
      gradle_version: config.safe_dig(:'build-tools', :gradle, :version) || '4.4.1',
      gradle_checksum: config.safe_dig(:'build-tools', :gradle, :checksum) || 'sha256:dd9b24950dc4fca7d1ca5f1ccd57ca8c5b9eb407e3e6e0f48174fde4bb19ed06'
    }
    @config = cfg.deep_stringify_keys
  end
end
