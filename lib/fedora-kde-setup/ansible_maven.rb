# frozen_string_literal: true

require 'fileutils'
require 'yaml'
require 'fedora-kde-setup/hash'
require 'fedora-kde-setup/util'

class AnsibleMaven
  include Util
  attr_reader :config

  def initialize(config)
    cfg = {
      maven_version: config.safe_dig(:'build-tools', :maven, :version) || '3.5.3',
      maven_is_default_installation: config.safe_dig(:'build-tools', :maven, :default) || true
    }
    @config = cfg.deep_stringify_keys
  end
end
