# frozen_string_literal: true

require 'fileutils'
require 'yaml'
require 'fedora-kde-setup/hash'
require 'fedora-kde-setup/util'

class AnsibleRBEnv
  include Util
  attr_reader :config

  def initialize(config)
    cfg = {
      rbenv: {
        env: 'user',
        rubies: ruby_versions(config),
        default_ruby: config.safe_dig(:rbenv, :default) || '2.4.0',
        version: 'v1.0.0'
      },
      rbenv_users: users(config)
    }
    @config = deep_stringify_keys(cfg)
  end

  private

  def ruby_versions(config)
    versions = config.safe_dig(:rbenv, :ruby_versions) || []
    if !versions.empty?
      versions.map { |v| deep_stringify_keys( version: v ) }
    else
      [{ version: '2.4.0' }, { version: '2.5.0' }]
    end
  end

  def users(config)
    config[:users].reject { |_k, v| v[:rbenv].nil? }.map { |k, _v| k.to_s }
  end
end
