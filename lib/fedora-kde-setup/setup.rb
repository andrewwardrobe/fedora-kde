# frozen_string_literal: true

class Setup
  @ideaU_de = <<~HEREDOC
    [Desktop Entry]
    Version=1.0
    Type=Application
    Name=IntelliJ IDEA Ultimate Edition
    Icon=/opt/ideaU/idea-IU-173.4301.25/bin/idea.png
    Exec="/opt/ideaU/idea-IU-173.4301.25/bin/idea.sh" %f
    Comment=The Drive to Develop
    Categories=Development;IDE;
    Terminal=false
    StartupWMClass=jetbrains-idea
  HEREDOC

  @rubymine_de = <<~HEREDOC
    [Desktop Entry]
    Version=1.0
    Type=Application
    Name=RubyMine
    Icon=/opt/rubymine/RubyMine-2017.3.2/bin/RMlogo.svg
    Exec="/opt/rubymine/RubyMine-2017.3.2/bin/rubymine.sh" %f
    Comment=The Drive to Develop
    Categories=Development;IDE;
    Terminal=false
    StartupWMClass=jetbrains-rubymine
  HEREDOC

  def symbolize_keys(hash)
    hash.each_with_object({}) do |(key, value), result|
      new_key = case key
                when String then key.to_sym
                else key
                end
      new_value = case value
                  when Hash then symbolize_keys(value)
                  else value
                  end
      result[new_key] = new_value
    end
  end

  def shortcuts(config)
    File.open('/usr/local/share/applications/jetbrains-idea.desktop', 'w') { |file| file.write @ideaU_de } if config[:intellij_ultimate] && config[:intellij_ultimate][:install]
    File.open('/usr/local/share/applications/jetbrains-rubymine.desktop', 'w') { |file| file.write @rubymine_de } if config[:rubymine] && config[:rubymine][:install]
  end

  def setup(cfg_file)
    config = load_config(cfg_file)

    UserSetup.new(config[:users]).do
    puppet_builder = PuppetBuilder.new(config)
    puppet_builder.build
    puppet_builder.apply unless $options[:nopuppet]
    shortcuts(config)
    ansible_builder = AnsibleBuilder.new config
    ansible_builder.build
    ansible_builder.execute if $options[:ansible]
  end

  def load_config(cfg_file)
    # unless File.exist? '/usr/share/locale/config.yaml'
    #  File.open('/usr/share/locale/config.yaml', 'w') do |file|
    #    file.write "---\ngettext:\n"
    # end
    # end
    config = symbolize_keys YAML.load_file(cfg_file)
  end

  def ansible_config; end

  def intelij_config
    config = load_config('/home/andrew/IdeaProjects/fedora-kde/spec/vagrant/script/data.yaml')
    builder = AnsibleBuilder.new config
    puts builder.host_vars.to_yaml
    puts builder.plays.to_yaml
  end
end
