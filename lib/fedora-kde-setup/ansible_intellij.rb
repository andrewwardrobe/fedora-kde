# frozen_string_literal: true

require 'fileutils'
require 'yaml'
require 'fedora-kde-setup/hash'
require 'fedora-kde-setup/util'

class AnsibleIntellij
  include Util
  attr_reader :config

  def initialize(config)
    cfg = {
      intellij_edition: config.safe_dig(:ides, :intellij, :edition) || 'community',
      intellij_version: config.safe_dig(:ides, :intellij, :version) || '2019.2'
    }
    users = []
    config[:users].each do |idx, user|
      ide_config = user.dig(:ides, :intellij)
      users << user_config(ide_config, idx) if ide_config
    end

    cfg[:users] = users
    @config = cfg.deep_stringify_keys
  end

  def user_config(config, user)
    conf = {
      username: user.to_s
    }

    conf[:intellij_plugins] = config[:plugins] if config[:plugins]
    conf[:intellij_jdks] = config[:jdks] if config[:jdks]
    conf[:intellij_default_jdk] = config[:default_jdk] if config[:default_jdk]
    conf[:intellij_license_key_path] = config[:license] || ''

    conf
  end
end
