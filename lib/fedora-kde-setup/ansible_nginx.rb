# frozen_string_literal: true

require 'fileutils'
require 'yaml'
require 'fedora-kde-setup/hash'
require 'fedora-kde-setup/util'

class AnsibleNginx
  include Util
  attr_reader :config

  def initialize(config)
    cfg = []
    cfg << deep_stringify_keys(directory)
    cfg << deep_stringify_keys(openssl_key)
    cfg << deep_stringify_keys(openssl_signing_request)
    cfg << deep_stringify_keys(openssl_certificate)
    cfg << deep_stringify_keys(container)
    @config = cfg
  end

  def directory
    {
      name: 'Make directory for certs',
      file: {
        state: 'directory',
        path: '/etc/ssl/certs'
      }
    }
  end

  def openssl_key
    {
      openssl_privatekey: {
        path: '/etc/ssl/certs/local.key'
      }
    }
  end

  def openssl_certificate
    {
      name: 'Generate an SSL Certificate',
      openssl_certificate: {
        path: '/etc/ssl/certs/local.crt',
        privatekey_path: '/etc/ssl/certs/local.key',
        csr_path: '/tmp/local.csr',
        provider: 'selfsigned'
      }
    }
  end
  def openssl_signing_request
    {
      name: 'Signing Request',
      openssl_csr: {
        path: '/tmp/local.csr',
        privatekey_path: '/etc/ssl/certs/local.key',
        subject_alt_name: 'DNS:localhost,DNS:localhost.local,DNS:*.local,DNS:*.localhost'
      }
    }
  end

  def container
    {
      name: 'Container is running',
      docker_container: {
        name: 'nginx',
        image: 'jwilder/nginx-proxy',
        ports: %w[80:80 443:443],
        restart_policy: 'always',
        privileged: 'yes',
        volumes: %w[/var/run/docker.sock:/tmp/docker.sock:ro /etc/ssl/certs:/etc/nginx/certs]
      }
    }
  end
end
