#!/usr/bin/env ruby
# frozen_string_literal: true

require 'yaml'
require 'etc'
require 'English'
require 'optparse'
require 'fileutils'
require 'erb'
require 'fedora-kde-setup/ansible_jenkins'
require 'fedora-kde-setup/ansible_builder'
require 'fedora-kde-setup/puppet'
require 'fedora-kde-setup/user'
require 'fedora-kde-setup/hash'
require 'fedora-kde-setup/setup'

$options = {
  dotfiles: true,
  rubymine: true,
  intellij: true,
  jenkins: true,
  mounts: true,
  ansible: true
}

OptionParser.new do |opts|
  opts.banner = 'Usage: setup.rb action [options] <datafile>'

  opts.on('--no-puppet', 'Don\'t Run Puppet') do
    $options[:nopuppet] = true
  end
  opts.on('--no-dotfiles', 'Skip Dot files') do
    $options[:dotfiles] = false
  end
  opts.on('--no-rubymine', 'Skip rubymine Install') do
    $options[:rubymine] = false
  end
  opts.on('--no-intellij', 'Skip Intellij Install') do
    $options[:intellij] = false
  end
  opts.on('--no-mounts', 'Skip Mounts') do
    $options[:mounts] = false
  end
  opts.on('--no-jenkins', 'Skip Jenkins') do
    $options[:jenkins] = false
  end
  opts.on('--no-ansible', 'Skip Jenkins') do
    $options[:ansible] = false
  end
end.parse!
